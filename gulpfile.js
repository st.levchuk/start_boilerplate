'use strict';

const gulp = require('gulp'),
  watch = require('gulp-watch'),
  browserify = require('browserify'),
  babelify = require('babelify'),
  prefixer = require('gulp-autoprefixer'),
  source = require('vinyl-source-stream'),
  uglify = require('gulp-uglify'),
  buffer = require('vinyl-buffer'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  rigger = require('gulp-rigger'),
  cleanCSS = require('gulp-clean-css'),
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant'),
  rimraf = require('rimraf'),
  spritesmith = require('gulp.spritesmith'),
  svgSprite = require('gulp-svg-sprite'),
  gulpSequence = require('gulp-sequence'),
  plumber = require('gulp-plumber'),
  browserSync = require("browser-sync"),
  htmlmin = require('gulp-html-minifier'),
  jsonminify = require('gulp-jsonminify'),
  reload = browserSync.reload;

const path = {
  build: {
    html: 'build/',
    js: 'build/js/',
    style: 'build/styles/',
    img: 'build/images/',
    fonts: 'build/fonts/',
    ajax: 'build/ajax/',
    php: 'build/php/',
    video: 'build/video/',
    json: 'build/json/',
    manifest: 'build/',
    sprite: 'build/images/'
  },
  src: {
    html: './src/**/*.html',
    js: './src/js/app.js',
    style: './src/styles/*.scss',
    img: './src/images/**/*.{gif,jpg,png,svg,ico}',
    fonts: './src/fonts/**/*.{eot,woff,woff2,svg}',
    ajax: './src/ajax/**/*.*',
    php: './src/php/**/*.*',
    video: './src/video/**/*.*',
    json: './src/json/*.json',
    manifest: './src/manifest.*'
  },
  watch: {
    html: './src/**/*.html',
    js: './src/js/**/*.js',
    style: './src/styles/**/*.scss',
    img: './src/images/**/*.{gif,jpg,png,svg,ico}',
    fonts: './src/fonts/**/*.{eot,woff,woff2,svg}',
    sprite: './src/sprite/*.*',
    spriteSvg: './src/svg-sprites/*.*',
    ajax: './src/ajax/**/*.*',
    php: './src/php/**/*.*',
    video: './src/video/**/*.*',
    json: './src/json/*.json',
    manifest: './src/manifest.*'
  },
  clean: './build'
};

const config = {
  server: {baseDir: "./build"},
  tunnel: false,
  host: 'localhost',
  port: 9000,
  logPrefix: "nordman_818"
};

gulp.task('webserver', function () {
  browserSync(config);
});

gulp.task('clean', function (cb) {
  rimraf(path.clean, cb);
});

gulp.task('html:build', function () {
  return gulp.src(path.src.html)
    .pipe(plumber())
    .pipe(rigger())
    .pipe(gulp.dest(path.build.html))
    .pipe(reload({stream: true}));
});

gulp.task('html:prod', function () {
  return gulp.src(path.src.html)
    .pipe(plumber())
    .pipe(rigger())
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest(path.build.html))
    .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
  browserify({entries: path.src.js, debug: true})
    .transform("babelify", {presets: ["es2015"]})
    .bundle()
    .on('error', console.error.bind(console))
    .pipe(source('app.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.build.js))
    .pipe(reload({stream: true}));
});
gulp.task('js:prod', function () {
  browserify({entries: path.src.js, debug: true})
    .transform("babelify", {presets: ["es2015"]})
    .bundle()
    .on('error', console.error.bind(console))
    .pipe(source('app.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest(path.build.js))
    .pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
  return gulp.src(path.src.style)
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass({
      sourceMap: true,
      errLogToConsole: true
    }))
    .pipe(prefixer({
      browsers: [
        'ie >= 11',
        'ie_mob >= 11',
        'ff >= 30',
        'chrome >= 34',
        'safari >= 7',
        'opera >= 23',
        'ios >= 6',
        'android >= 4.4',
        'bb >= 10'
      ],
      cascade: false
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.build.style))
    .pipe(reload({stream: true}));
});

gulp.task('style:prod', function () {
  return gulp.src(path.src.style)
    .pipe(plumber())
    .pipe(sass({
      sourceMap: false,
      errLogToConsole: true
    }))
    .pipe(prefixer({
      browsers: [
        'ie >= 11',
        'ie_mob >= 11',
        'ff >= 30',
        'chrome >= 34',
        'safari >= 7',
        'opera >= 23',
        'ios >= 6',
        'android >= 4.4',
        'bb >= 10'
      ],
      cascade: false
    }))
    .pipe(cleanCSS())
    .pipe(gulp.dest(path.build.style))
    .pipe(reload({stream: true}));
});
gulp.task('image:build', function () {
  return gulp.src(path.src.img)
    .pipe(plumber())
    .pipe(gulp.dest(path.build.img))
    .pipe(reload({stream: true}));
});
gulp.task('image:prod', function () {
  return gulp.src(path.src.img)
    .pipe(plumber())
    .pipe(imagemin({
      progressive: true,
      interlaced: true,
      pngquant: true,
      verbose: true
    }))
    .pipe(gulp.dest(path.build.img))
    .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function () {
  return gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.build.fonts))
    .pipe(reload({stream: true}));
});

gulp.task('ajax:build', function () {
  return gulp.src(path.src.ajax)
    .pipe(gulp.dest(path.build.ajax))
    .pipe(reload({stream: true}));

});

gulp.task('php:build', function () {
  return gulp.src(path.src.php)
    .pipe(gulp.dest(path.build.php))
    .pipe(reload({stream: true}));

});

gulp.task('video:build', function () {
  return gulp.src(path.src.video)
    .pipe(gulp.dest(path.build.video))
    .pipe(reload({stream: true}));

});

gulp.task('json:build', function () {
  return gulp.src(path.src.json)
    .pipe(jsonminify())
    .pipe(gulp.dest(path.build.json))
    .pipe(reload({stream: true}));

});

gulp.task('manifest:build', function () {
  return gulp.src(path.src.manifest)
    .pipe(gulp.dest(path.build.manifest))
    .pipe(reload({stream: true}));

});

gulp.task('sprite:build', function () {
  let spriteData =
    gulp.src('./src/sprite/*.*')
      .pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.scss',
        cssFormat: 'scss',
        algorithm: 'diagonal',
        imgPath: '../images/sprite.png'
      }));

  spriteData.img.pipe(gulp.dest(path.build.sprite));
  spriteData.css.pipe(gulp.dest('./src/styles/sprite/'));
});

gulp.task('svgSprite:build', function () {
  return gulp.src('src/svg-sprites/*.svg')
    .pipe(svgSprite({
      shape: {
        spacing: {
          padding: 5
        }
      },
      mode: {
        css: {
          dest: "./",
          layout: "diagonal",
          sprite: 'svg-sprite.svg',
          bust: false,
          render: {
            scss: {
              dest: "../../src/styles/sprite/svg_sprite.scss",
              template: "src/styles/sprite/sprite-template.scss"
            }
          }
        }
      },
      variables: {
        mapname: "icons"
      }
    }))
    .pipe(gulp.dest('build/images'));
});

gulp.task('sprite', [
  'sprite:build'
]);

gulp.task('build', gulpSequence(
  [
    'svgSprite:build',
    'sprite:build'
  ],
  'image:build',
  'video:build',
  'style:build',
  [
    'js:build',
    'fonts:build',
    'ajax:build',
    'php:build',
    'json:build',
    'manifest:build'
  ],
  'html:build'
));

gulp.task('build:prod', gulpSequence(
  [
    'svgSprite:build',
    'sprite:build'
  ],
  'image:build',
  'video:build',
  'style:prod',
  [
    'js:prod',
    'fonts:build',
    'ajax:build',
    'php:build',
    'json:build',
    'manifest:build'
  ],
  'html:prod'
));

gulp.task('watch', function () {
  watch([path.watch.html], function (event, cb) {
    gulp.start('html:build');
  });
  watch([path.watch.style], function (event, cb) {
    gulp.start('style:build');
  });
  watch([path.watch.js], function (event, cb) {
    gulp.start('js:build');
  });
  watch([path.watch.img], function (event, cb) {
    gulp.start('image:build');
  });
  watch([path.watch.fonts], function (event, cb) {
    gulp.start('fonts:build');
  });
  watch([path.watch.ajax], function (event, cb) {
    gulp.start('ajax:build');
  });
  watch([path.watch.php], function (event, cb) {
    gulp.start('php:build');
  });
  watch([path.watch.video], function (event, cb) {
    gulp.start('video:build');
  });
  watch([path.watch.json], function (event, cb) {
    gulp.start('json:build');
  });
  watch([path.watch.manifest], function (event, cb) {
    gulp.start('manifest:build');
  });
  watch([path.watch.sprite], function (event, cb) {
    gulp.start('sprite:build');
  });
  watch([path.watch.spriteSvg], function (event, cb) {
    gulp.start('svgSprite:build');
  });
});

gulp.task('default', gulpSequence('build', 'watch', 'webserver'));
gulp.task('prod-build', gulpSequence('clean', 'build:prod'));

