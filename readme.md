# Setup

## install
```sh
$ npm install
```

## npm tasks
- dev
- clean

## start
type in command line
```sh
$ npm dev
```
Visit http://localhost:9000/ from your browser of choice. 

## start
src - source files  
build - production files (generated from src files)